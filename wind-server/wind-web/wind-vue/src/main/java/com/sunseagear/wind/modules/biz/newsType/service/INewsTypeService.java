package com.sunseagear.wind.modules.biz.newsType.service;

import com.sunseagear.common.mvc.service.ICommonService;
import com.sunseagear.wind.modules.biz.newsType.entity.NewsType;

/**
 * All rights Reserved, Designed By www.sunseagear.com
 *
 * @version V1.0
 * @package biz.newsType
 * @title: 新闻类型控制器
 * @description: 新闻类型控制器
 * @author:
 * @date: 2020-11-09 04:00:26
 * @copyright: www.sunseagear.com Inc. All rights reserved.
 */
public interface INewsTypeService extends ICommonService<NewsType> {

}
