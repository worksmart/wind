package com.sunseagear.wind.modules.biz.employee.service;

import com.sunseagear.common.mvc.service.ICommonService;
import com.sunseagear.wind.modules.biz.employee.entity.Employee;

/**
 * All rights Reserved, Designed By www.sunseagear.com
 *
 * @version V1.0
 * @package biz.employee
 * @title: 员工信息&mdash;&mdash;左树右表主表（飞廉演示）控制器
 * @description: 员工信息&mdash;&mdash;左树右表主表（飞廉演示）控制器
 * @author:
 * @date: 2021-05-20 14:04:18
 * @copyright: www.sunseagear.com Inc. All rights reserved.
 */
public interface IEmployeeService extends ICommonService<Employee> {

}
