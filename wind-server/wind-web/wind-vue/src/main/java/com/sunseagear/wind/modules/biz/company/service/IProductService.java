package com.sunseagear.wind.modules.biz.company.service;

import com.sunseagear.common.mvc.service.ICommonService;
import com.sunseagear.wind.modules.biz.company.entity.Product;

/**
 * All rights Reserved, Designed By www.sunseagear.com
 *
 * @version V1.0
 * @package biz.company
 * @title: 产品管理&mdash;&mdash;级联表子表（飞廉演示）控制器
 * @description: 产品管理&mdash;&mdash;级联表子表（飞廉演示）控制器
 * @author:
 * @date: 2021-10-21 07:59:27
 * @copyright: www.sunseagear.com Inc. All rights reserved.
 */
public interface IProductService extends ICommonService<Product> {

}
