package com.sunseagear.wind.modules.biz.newsType.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sunseagear.wind.modules.biz.newsType.entity.NewsType;
import org.apache.ibatis.annotations.Mapper;

/**
 * All rights Reserved, Designed By www.sunseagear.com
 *
 * @version V1.0
 * @package biz.newsType
 * @title: 新闻类型Mapper
 * @description: 新闻类型Mapper
 * @author:
 * @date: 2020-11-09 04:00:26
 * @copyright: www.sunseagear.com Inc. All rights reserved.
 */
@Mapper
public interface NewsTypeMapper extends BaseMapper<NewsType> {

}
