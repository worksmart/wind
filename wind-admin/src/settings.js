const defaultSettings = {
  title: '欢迎使用飞廉',
  showSettings: true,
  topMenu: false,
  tagsView: true,
  fixedHeader: false,
  sidebarLogo: true,
  errorLog: 'production'
}
export default defaultSettings
